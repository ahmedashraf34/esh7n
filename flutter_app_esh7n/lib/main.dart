import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_esh7n/service/LocalizationService.dart';
import 'package:flutter_app_esh7n/service/PrefService.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'AppLocalizations.dart';
import 'app_setting/locator.dart';
import 'app_setting/router.dart';

String getAppId() {
  if (Platform.isIOS) {
    return 'ca-app-pub-3940256099942544~1458002511';
  } else if (Platform.isAndroid) {
    return 'ca-app-pub-3940256099942544~3347511713';
  }
  return null;
}

main() async {
  await setupLocator();
  await Admob.initialize(getAppId());
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> {
  var localService = locator<LocalizationService>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    localService.onLocaleChanged = onLocaleChange;
  }

  onLocaleChange(String countryCode) {
    setState(() {
      localService.changeLocal(countryCode);
    });
  }

  @override
  Widget build(BuildContext context) {
    var pref = locator<PrefService>();

    // TODO: implement build
    return MultiProvider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'esh7nly',
        locale: Locale(pref.userLocal),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          FallbackCupertinoLocalisationsDelegate(),
          localService.currentLocal
        ],
        supportedLocales: [
          Locale("en"),
          Locale("ar")
          // OR Locale('ar', 'AE') OR Other RTL locales
        ],
        onGenerateRoute: Router.generateRoute,
      ),
      providers: [],
    );
  }
}
