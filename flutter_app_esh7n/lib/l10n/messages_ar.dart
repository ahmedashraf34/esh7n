// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "turnFlash" : MessageLookupByLibrary.simpleMessage("تشغيل الفلاش"),
    "textHint" : MessageLookupByLibrary.simpleMessage("اضغط علي الكاميرا للفحص او قم بادخال الارقام"),
    "flashHint" : MessageLookupByLibrary.simpleMessage("ينصح بها في الاضاءة الضعيفة"),
    "chargeNow" : MessageLookupByLibrary.simpleMessage("اشحن الان"),
    "changeSim" : MessageLookupByLibrary.simpleMessage("تغيير نوع الشريحة"),
    "languageToolTip" : MessageLookupByLibrary.simpleMessage("تغيير اللغة"),
    "cameraToolTip" : MessageLookupByLibrary.simpleMessage("اضغط لفحص الكارت"),
    "textValidation1" : MessageLookupByLibrary.simpleMessage("يجب عليك ادخال الارقام اولا"),
    "textValidation2" : MessageLookupByLibrary.simpleMessage("يجب عليك ادخال كل ارقام الشحن الموجودة بالكارت"),
    "splashText" : MessageLookupByLibrary.simpleMessage("وفر مجهود وهنشحلك الكارت"),
    "appName" : MessageLookupByLibrary.simpleMessage("اشحنلي"),

  };
}
