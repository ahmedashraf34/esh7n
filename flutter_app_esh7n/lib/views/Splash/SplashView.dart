import 'package:flutter/material.dart';
import 'package:flutter_app_esh7n/Views/ChooseSim/ChooseSimView.dart';
import 'package:flutter_app_esh7n/views_model/SplashViewModel.dart';

import '../../AppLocalizations.dart';
import '../_BaseView.dart';

class SplashView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SplashViewState();
  }
}

class _SplashViewState extends State<SplashView> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BaseView<SplashViewModel>(
      onModelReady: (model) {
        model.onHideStatusBar();
        Future.delayed(Duration(seconds: 2),
            () => Navigator.pushReplacementNamed(context, "/ChooseSim"));
      },
      builder: (mContext, model, child) => Scaffold(
            body: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).appName,
                    style: TextStyle(
                      fontSize: 40,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Image.asset("assets/sim.png"),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Text(
                      AppLocalizations.of(context).splashText,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.grey[600],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
    );
  }
}
