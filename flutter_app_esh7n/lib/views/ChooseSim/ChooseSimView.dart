import 'package:flutter/material.dart';
import 'package:flutter_app_esh7n/views_model/ChooseSimViewModel.dart';
import '../_BaseView.dart';

class ChooseSimView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ChooseSimViewState();
  }
}

class _ChooseSimViewState extends State<ChooseSimView> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BaseView<ChooseSimViewModel>(
      onModelReady: (model) {
        model.onShowStatusBar();
      },
      builder: (mContext, model, child) => Scaffold(
            body: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                      child: GestureDetector(
                    onTap: () {
                      model.pref.CurrentCheckOrange = true;
                      Map<String, dynamic> newData = {
                        "color": Colors.orange,
                        "name": "orange",
                        "code": "102"
                      };
                      Navigator.pushReplacementNamed(context, "/Home",
                          arguments: newData);
                    },
                    child: Image.asset(
                      "assets/orange.png",
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width,
                    ),
                  )),
                  Flexible(
                      child: GestureDetector(
                    onTap: () {
                      model.pref.CurrentCheckEtisalat = true;

                      Map<String, dynamic> newSet = {
                        "color": Colors.green,
                        "name": "etisalat",
                        "code": "556"
                      };
                      Navigator.pushReplacementNamed(context, "/Home",
                          arguments: newSet);
                    },
                    child: Image.asset(
                      "assets/etisalat.jpg",
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width,
                    ),
                  )),
                  Flexible(
                      child: GestureDetector(
                    onTap: () {
                      model.pref.CurrentCheckVoda = true;

                      Map<String, dynamic> newSet = {
                        "color": Colors.red,
                        "name": "vodaphone",
                        "code": "858"
                      };
                      Navigator.pushReplacementNamed(context, "/Home",
                          arguments: newSet);
                    },
                    child: Image.asset(
                      "assets/voda.jpg",
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width,
                    ),
                  )),
                  Flexible(
                      child: GestureDetector(
                    onTap: () {
                      model.pref.CurrentCheckWe = true;

                      Map<String, dynamic> newSet = {
                        "color": Colors.purple,
                        "name": "we",
                        "code": "555"
                      };
                      Navigator.pushReplacementNamed(context, "/Home",
                          arguments: newSet);
                    },
                    child: Image.asset(
                      "assets/we.png",
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width,
                    ),
                  )),
                ],
              ),
            ),
          ),
    );
  }
}
