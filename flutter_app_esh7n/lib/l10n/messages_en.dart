// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "turnFlash" : MessageLookupByLibrary.simpleMessage("Turn on the flash"),
    "textHint" : MessageLookupByLibrary.simpleMessage("Tap the camera to check or enter the numbers"),
    "flashHint" : MessageLookupByLibrary.simpleMessage("Recommended in weak lighting"),
    "chargeNow" : MessageLookupByLibrary.simpleMessage("Charge now"),
    "changeSim" : MessageLookupByLibrary.simpleMessage("Change the sim type"),
    "languageToolTip" : MessageLookupByLibrary.simpleMessage("Change the language"),
    "cameraToolTip" : MessageLookupByLibrary.simpleMessage("Click to check the card"),
    "textValidation1" : MessageLookupByLibrary.simpleMessage("You must enter the numbers first"),
    "textValidation2" : MessageLookupByLibrary.simpleMessage("You must enter all numbers on the card"),
    "splashText" : MessageLookupByLibrary.simpleMessage("Save effort and we will charge the card"),
    "appName" : MessageLookupByLibrary.simpleMessage("esh7nly"),
  };
}
