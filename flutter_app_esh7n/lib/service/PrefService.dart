import 'package:shared_preferences/shared_preferences.dart';
class PrefService{

  static SharedPreferences _preferences;
  static PrefService _instance;


  static Future<PrefService> getInstance() async {
    if (_instance == null) {
      _instance = PrefService();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance;
  }

  //user Local

  String get userLocal => _preferences.getString("userLocal") ?? "ar";

  set userLocal(String value) => _preferences.setString("userLocal", value);


  //Current CheckOrange

  bool get CurrentCheckOrange => _preferences.getBool("CurrentCheckOrange") ?? false;

  set CurrentCheckOrange(bool value) => _preferences.setBool("CurrentCheckOrange", value);


  //Current CheckEtisalat

  bool get CurrentCheckEtisalat => _preferences.getBool("CurrentCheckEtisalat") ?? false;

  set CurrentCheckEtisalat(bool value) => _preferences.setBool("CurrentCheckEtisalat", value);

  //Current CheckVodaa

  bool get CurrentCheckVoda => _preferences.getBool("CurrentCheckVoda") ?? false;

  set CurrentCheckVoda(bool value) => _preferences.setBool("CurrentCheckVoda", value);

  //Current CheckWe

  bool get CurrentCheckWe => _preferences.getBool("CurrentCheckWe") ?? false;

  set CurrentCheckWe(bool value) => _preferences.setBool("CurrentCheckWe", value);



}