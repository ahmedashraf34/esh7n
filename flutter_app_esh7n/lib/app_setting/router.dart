import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_esh7n/Views/Splash/SplashView.dart';
import 'package:flutter_app_esh7n/views/ChooseSim/ChooseSimView.dart';
import 'package:flutter_app_esh7n/views/Home/HomeView.dart';
import 'package:flutter_app_esh7n/service/PrefService.dart';

import 'locator.dart';

AnimationController controller;

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) {
          return SplashView();
        });
      case '/ChooseSim':
        return MaterialPageRoute(builder: (_) {
          var pref = locator<PrefService>();
          if (pref.CurrentCheckOrange) {
            return HomeView(
              simCode: "102",
              simName: "orange",
              simColor: Colors.orange,
            );
          } else if (pref.CurrentCheckWe) {
            return HomeView(
              simCode: "555",
              simName: "we",
              simColor: Colors.purple,
            );
          } else if (pref.CurrentCheckVoda) {
            return HomeView(
              simCode: "858",
              simName: "vodaphone",
              simColor: Colors.red,
            );
          } else if (pref.CurrentCheckEtisalat) {
            return HomeView(
              simCode: "556",
              simName: "etisalat",
              simColor: Colors.green,
            );
          } else {
            return ChooseSimView();
          }
        });
      case '/Home':
        return MaterialPageRoute(builder: (_) {
          var arguments = settings.arguments as Map<String, dynamic>;
          return HomeView(
            simColor: arguments["color"],
            simName: arguments["name"],
            simCode: arguments["code"],
          );
        });

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                  child: Text('No route defined for ${settings.name}'),
                ),
              ),
        );
    }
  }
}
