import 'dart:async';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'l10n/messages_all.dart';

class AppLocalizations {
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations();
    });
  }

  //Put Your Strings

  String get turnFlash {
    return Intl.message('turnFlash', name: 'turnFlash');
  }

  String get textHint {
    return Intl.message('textHint', name: 'textHint');
  }

  String get flashHint {
    return Intl.message('flashHint', name: 'flashHint');
  }

  String get chargeNow {
    return Intl.message('chargeNow', name: 'chargeNow');
  }

  String get changeSim {
    return Intl.message('changeSim', name: 'changeSim');
  }

  String get languageToolTip {
    return Intl.message('languageToolTip', name: 'languageToolTip');
  }

  String get cameraToolTip {
    return Intl.message('cameraToolTip', name: 'cameraToolTip');
  }

  String get textValidation1 {
    return Intl.message('textValidation1', name: 'textValidation1');
  }

  String get textValidation2 {
    return Intl.message('textValidation2', name: 'textValidation2');
  }

  String get splashText {
    return Intl.message('splashText', name: 'splashText');
  }

  String get appName {
    return Intl.message('appName', name: 'appName');
  }
}

class SpecificLocalizationDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  final Locale overriddenLocale;

  SpecificLocalizationDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) => overriddenLocale != null;

  @override
  Future<AppLocalizations> load(Locale locale) =>
      AppLocalizations.load(overriddenLocale);

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => true;
}

class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      SynchronousFuture<_DefaultCupertinoLocalizations>(
          _DefaultCupertinoLocalizations(locale));

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}

class _DefaultCupertinoLocalizations extends DefaultCupertinoLocalizations {
  final Locale locale;

  _DefaultCupertinoLocalizations(this.locale);
}
