import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_esh7n/service/LocalizationService.dart';
import 'package:flutter_app_esh7n/service/PrefService.dart';
import 'package:provider/provider.dart';
import '../app_setting/locator.dart';


class BaseViewModel extends ChangeNotifier {
  var pref = locator<PrefService>();
  var localService = locator<LocalizationService>();




  onChangeLang(String countryCode) {
    localService.onLocaleChanged(countryCode);
    notifyListeners();

  }
  onShowStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  onHideStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }



  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double screenHeight(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).height / dividedBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).width / dividedBy;
  }
}
