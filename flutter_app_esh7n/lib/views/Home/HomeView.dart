import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_esh7n/views_model/HomeViewModel.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../../AppLocalizations.dart';
import '../_BaseView.dart';

class HomeView extends StatefulWidget {
  Color simColor;
  String simName;
  String simCode;

  HomeView({this.simColor, this.simName, this.simCode});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeViewState();
  }
}

class _HomeViewState extends State<HomeView> {
  Size _previewOcr;
  int _cameraOcr = FlutterMobileVision.CAMERA_BACK;
  bool _torchOcr = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> key = GlobalKey<FormState>();
  List<OcrText> texts;
  String text = "";
  bool isSelected = false;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BaseView<HomeViewModel>(
      onModelReady: (model) {
        model.onShowStatusBar();
        FlutterMobileVision.start().then((previewSizes) => setState(() {
              _previewOcr = previewSizes[_cameraOcr].first;
            }));
      },
      builder: (mContext, model, child) => Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: Tooltip(
                    child: IconButton(
                      icon: Icon(
                        (Icons.language),
                        size: 30,
                      ),
                      onPressed: () {
                        if (model.pref.userLocal == "en") {
                          model.onChangeLang("ar");
                        } else {
                          model.onChangeLang("en");
                        }
                      },
                    ),
                    message: AppLocalizations.of(context).languageToolTip,
                  ),
                ),
              ],
              backgroundColor: widget.simColor,
              title: Text(
                widget.simName,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              centerTitle: true,
            ),
            floatingActionButton: FloatingActionButton(
              tooltip: AppLocalizations.of(context).cameraToolTip,
              clipBehavior: Clip.hardEdge,
              backgroundColor: widget.simColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.linked_camera,
                  size: 30,
                ),
              ),
              onPressed: _read,
            ),
            body: buildSingleChildScrollView(context, model),
          ),
    );
  }

  SingleChildScrollView buildSingleChildScrollView(
      BuildContext context, HomeViewModel model) {
    return SingleChildScrollView(
      child: Form(
        key: key,
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: AdmobBanner(
                  adUnitId: getBannerAdUnitId(),
                  adSize: AdmobBannerSize.MEDIUM_RECTANGLE,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              buildTextFiled(context),
              buildFlashButton(context),
              buildButtons(model, context),
              SizedBox(
                height: 15,
              ),
              Container(
                child: AdmobBanner(
                  adUnitId: getBannerAdUnitId(),
                  adSize: AdmobBannerSize.BANNER,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                child: AdmobBanner(
                  adUnitId: getBannerAdUnitId(),
                  adSize: AdmobBannerSize.BANNER,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Row buildButtons(HomeViewModel model, BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Flexible(
          child: GestureDetector(
            onTap: () {
              model.pref.CurrentCheckOrange = false;
              model.pref.CurrentCheckVoda = false;
              model.pref.CurrentCheckEtisalat = false;
              model.pref.CurrentCheckWe = false;
              Navigator.pushReplacementNamed(context, "/ChooseSim");
            },
            child: Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: widget.simColor,
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: FittedBox(
                child: Row(
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).changeSim,
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    RotatedBox(
                      child: Image.asset(
                        "assets/sim-card.png",
                        height: 30,
                        width: 30,
                      ),
                      quarterTurns: 3,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Flexible(
          child: GestureDetector(
            onTap: () {
              key.currentState.save();
              if (text == "") {
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text(
                    AppLocalizations.of(context).textValidation1,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  backgroundColor: widget.simColor,
                ));
              } else if (text.length < 15) {
                _scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    content: Text(
                      AppLocalizations.of(context).textValidation2,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    backgroundColor: widget.simColor,
                  ),
                );
              } else {
                MakeCall();
              }
            },
            child: Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: widget.simColor,
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: Text(
                AppLocalizations.of(context).chargeNow,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ],
    );
  }

  SwitchListTile buildFlashButton(BuildContext context) {
    return SwitchListTile(
        selected: isSelected,
        subtitle: Text(
          AppLocalizations.of(context).flashHint,
          style: TextStyle(color: Colors.grey[800]),
        ),
        title: Text(
          AppLocalizations.of(context).turnFlash,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
        ),
        value: _torchOcr,
        activeColor: widget.simColor,
        onChanged: (value) {
          setState(() {
            if (_torchOcr = value) {
              isSelected = true;
            } else {
              isSelected = false;
            }
            ;
          });
        });
  }

  Padding buildTextFiled(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Colors.white,
          border: Border.all(color: widget.simColor),
        ),
        child: TextFormField(
          inputFormatters: [
            LengthLimitingTextInputFormatter(15),
          ],
          initialValue: text,
          keyboardType: TextInputType.number,
          cursorColor: widget.simColor,
          style: TextStyle(color: Colors.black),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: AppLocalizations.of(context).textHint,
            hintStyle: TextStyle(
              fontSize: 17,
              color: Colors.grey[800],
            ),
          ),
          onSaved: (value) {
            setState(() {
              text = value;
            });
          },
        ),
      ),
    );
  }

  Future<Null> _read() async {
    List<OcrText> texts = [];
    try {
      texts = await FlutterMobileVision.read(
        flash: _torchOcr,
        autoFocus: true,
        multiple: false,
        waitTap: true,
        showText: true,
        preview: _previewOcr,
        camera: _cameraOcr,
        fps: 2.0,
      );
      setState(() {
        text = texts[0].value;
      });
    } on Exception {
      texts.add(OcrText('Failed to recognize text'));
      setState(() {
        text = texts[0].value;
      });
    }
  }

  void MakeCall() {
    UrlLauncher.launch('tel:${"*${widget.simCode}*" + text + "%23"}');
  }

  String getBannerAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/2934735716';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/6300978111';
    }
    return null;
  }


}
