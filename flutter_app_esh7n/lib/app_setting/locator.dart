import 'package:flutter_app_esh7n/service/LocalizationService.dart';
import 'package:flutter_app_esh7n/views_model/ChooseSimViewModel.dart';
import 'package:flutter_app_esh7n/views_model/HomeViewModel.dart';
import 'package:flutter_app_esh7n/views_model/SplashViewModel.dart';
import 'package:get_it/get_it.dart';

import 'package:flutter_app_esh7n/service/PrefService.dart';

GetIt locator;

void setupLocator() async {
  locator = GetIt();
  locator.registerFactory(() => SplashViewModel());
  locator.registerFactory(() => ChooseSimViewModel());
  locator.registerFactory(() => HomeViewModel());
  locator.registerLazySingleton(() => LocalizationService());

  var prefInstance = await PrefService.getInstance();
  locator.registerSingleton<PrefService>(prefInstance);
}
